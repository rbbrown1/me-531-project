
import numpy as np

class Accelerometer:
    
    def __init__(self, noise_mn, noise_var):
        self.noise_mn = noise_mn
        self.noise_var = noise_var

        self.v = 0.05

    def update_sensor(self, dt, v):
        noise = (np.random.default_rng().normal(float(self.noise_mn), self.noise_var))

        # The real sensor class would take in the previous v.
        # integrate the acceleration, then get the new velocity

        self.v = v+noise*dt

    def get_measurement(self):
        return self.v