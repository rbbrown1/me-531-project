import numpy as np


class Gyro:
    """ A wheel encoder class for a differential drive robot
    """

    def __init__(self, WheelDiameter=0.025, ugvWidth=0.06):
        """ Init function. All units in meters except ticks per rev and angular (degrees/sec).
            Default parameters reflect HMT Centurion
        """
        self.WheelD = WheelDiameter
        self.circumference = self.WheelD * np.pi
        self.width = ugvWidth
        self.measurement = 0

        self.w_std = 0.008

    def update_sensor(self, w):
        noiseW = (np.random.default_rng().normal(0, self.w_std))
        self.measurement = w + noiseW

    def get_measurement(self):
        return self.measurement

    def reset_angular(self):
        self.angular = 0
