import numpy as np

class Encoder:
    """ A wheel encoder class for a differential drive robot
    """
    def __init__(self,WheelDiameter = 0.025, ugvWidth = 0.06, ticksPerRev = 18):
        """ Init function. All units in meters except ticks per rev.
            Default parameters reflect HMT Centurion
        """        
        self.WheelD = WheelDiameter
        self.circumference = self.WheelD * np.pi
        self.width = ugvWidth
        self.ticksPerRev  = ticksPerRev
        self.RTicks = 0
        self.LTicks = 0

        self.w_std = 0.001
        self.v_std = 0.001

        self.w_std = 0.0
        self.v_std = 0.0

        self.w = 0
        self.v = 0



    def update_sensor(self, v, w, dt):
        # add noise to input state so we can simulate a noisey sensor
        v += (np.random.default_rng().normal(0, self.v_std))
        w += (np.random.default_rng().normal(0, self.w_std))

        # self.v = v
        # self.w = w

        vright =  (self.width / 2) * w + v
        vleft = vright - (self.width * w)

        dist_right = vright * dt
        dist_left = vleft * dt

        ratio_r = dist_right / self.circumference
        ratio_l = dist_left / self.circumference

        ticksR = 0
        ticksL = 0
        while(ratio_r > 1):
            ticksR = ticksR + self.ticksPerRev
            ratio_r = ratio_r - 1

        while(ratio_l > 1):
            ticksL = ticksL + self.ticksPerRev
            ratio_l = ratio_l - 1

        ticksR = ticksR + np.floor(ratio_r * self.ticksPerRev)
        ticksL = ticksL + np.floor(ratio_l * self.ticksPerRev)

        self.RTicks = self.RTicks + ticksR
        self.LTicks = self.LTicks + ticksL
           
    def get_measurement(self):

        # return (self.v, self.w)
        return (self.LTicks, self.RTicks)

    def reset_ticks(self):
        self.RTicks = 0
        self.LTicks = 0

