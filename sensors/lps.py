from sensors.anchor import Anchor
import numpy as np
import logging

import yaml
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

SPEED_OF_LIGHT = 299792458.0

class Lps:

    def __init__(self, anchor_num, anchor_pos_file=None, anchor_pos_tuple=None):
        self.log = logging.getLogger(__name__)
        logging.basicConfig(format='%(levelname)s:%(name)s:%(funcName)s: %(message)s')
        if anchor_num < 6 or anchor_num > 8:
            self.log.critical("LPS only supports between 6 and 8 anchors!")
            raise ValueError("Number of anchors out of bounds")
        if anchor_pos_file is None and anchor_pos_tuple is None:
            self.log.critical("No anchor positions given. Either pass a file name or a list of tuples.")
            raise ValueError("Anchor Positions Unknown")
        if anchor_pos_tuple is not None and len(anchor_pos_tuple) != anchor_num:
            self.log.critical("Anchor numbers and anchor positions misaligned. The number of anchors and positions must match.")
            raise ValueError("Anchor, Position Misalignment")

        anchorStream = None
        anchorData = None
        if anchor_pos_file is not None:
            try:
                anchorStream = open(anchor_pos_file,'r')
                anchorData = load(anchorStream, Loader=Loader)
            except yaml.YAMLError:
                self.log.critical("Anchor file invalid")
                raise ValueError("Anchor file missing or improperly formatted")
        self.num_anchors = anchor_num
        self.anchors = []
 
        if anchor_pos_tuple is not None:
            anchorData = anchor_pos_tuple
            for i in range(anchor_num):
                self.anchors.append(Anchor(id=i, x=anchorData[(i)][0], y = anchorData[(i)][1], z = anchorData[(i)][2])) 
        else:
            for i in range(anchor_num):
                self.anchors.append(Anchor(id=i, x=anchorData[(i)]['x'], y = anchorData[(i)]['y'], z = anchorData[(i)]['z'])) 

        self.log.info("LPS Class instantiated. ")
        self.anchor_send_ind = 0

    def GetAnchor(self, id):
       if id >= 0 and id < self.num_anchors:
           return self.anchors[id]
    
    def norm_diff(self, anchor_id, pose):
        anchor = self.anchors[anchor_id]
        dx =  pose[0] - anchor.x
        dy =  pose[1] - anchor.y
        dz =  pose[2] - anchor.z

        return np.sqrt(dx**2 + dy ** 2 + dz ** 2)

    

    def GetAnchorsTdoa2(self, pos, std=0.15*(10**(-9)), time=0):
        """ Get distance difference based on the time
            and the anchor that should be transmitting
            at this moment.
        """
        # Anchors get 16ms intervals to broadcast. Assume time in seconds
        # broadcast_anchor = int(np.floor((time / 0.016) % self.num_anchors))
        broadcast_anchor = int(np.floor(np.random.randint(0, 7)))
        # broadcast_anchor = self.anchor_send_ind
        # self.anchor_send_ind = self.anchor_send_ind + 1
        # if self.anchor_send_ind > (self.num_anchors-1):
        #     self.anchor_send_ind = 0

        last_anchor = broadcast_anchor - 1
        if last_anchor < 0:
            last_anchor = last_anchor + self.num_anchors
        # print("STD: {}".format(std))
        last_diff_time = self.norm_diff(last_anchor, pos) / SPEED_OF_LIGHT
        broad_diff_time = self.norm_diff(broadcast_anchor, pos) / SPEED_OF_LIGHT
        tdoa_meas = last_diff_time - broad_diff_time + np.random.default_rng().normal(0, std)
        return [[self.anchors[broadcast_anchor].x, self.anchors[broadcast_anchor].y, self.anchors[broadcast_anchor].z],
                [self.anchors[last_anchor].x, self.anchors[last_anchor].y, self.anchors[last_anchor].z],
                [tdoa_meas * SPEED_OF_LIGHT]]

    def __str__(self):
        return str(self.__class__.__name__)