import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from EKF import EKF
import json
from sensors.lps import Lps
from sensors.encoder import Encoder
from sensors.accelerometer import Accelerometer
from sensors.gyroscope import Gyro
import logging
import LQR
from datetime import datetime
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

"""https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console"""
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

CURRENT_TIME = 0
pos_sys = Lps(8, "./anchor_positions.yaml")
R = 0.065 / 2
V_MAX = 0.09
W_MAX = V_MAX / R
CTRLR_UPDATE_DIVISOR = 5
SEC_PER_TICK = 0.02

class Robot:
    """ Robot base class for generic robot
    """
    def __init__(self, x=0, y=0, theta=0, kp = 1):
        self.x = x
        self.y = y
        self.theta = theta
        self.Kp = kp
        self.trajectoryX = []
        self.trajectoryY= []
        self.trajectoryTh=[]
        self.noisyX = []
        self.noisyY = []
        self.GTx = []
        self.GTy = []
        self.GTth = []
        self.positional_dataframe = None
        self.estimator = None

        self.estDistToGoal = []
        self.gtDistToGoal = []
        self.dt = []

        # for process noise when moving around
        self.v_std = .01
        self.w_std = .1

    def Estimate(self):
        pass

    def ForwardKinematics(self):
        pass
    def RunController(self):
        pass
    def PlotTrajectory(self):

        # ----- estimated and ground truth trajectory -----
        fig, ax = plt.subplots()
        # ax[0].plot(self.trajectoryX, self.trajectoryY)
        #ax.plot(self.trajectoryX[1000:], self.trajectoryY[1000:])
        ax.plot(self.trajectoryX, self.trajectoryY, label='Estimated')
        #ax[0].set_title("Estimated Trajectory")
        ax.plot(self.GTx,self.GTy, label="Ground Truth")
        #ax[1].set_title("Ground Truth Trajectory")
        ax.scatter([0], [0], color='g', label='start', s=30)
        ax.scatter([3], [3], color='r', label='goal', s=30)
        ax.legend()

        # # ----- Error broken down by state variable -----
        # fig, ax = plt.subplots(3,1)
        # ax[0].plot(np.array(self.trajectoryX) - np.array(self.GTx))#, self.trajectoryY[:i])
        # # ax[0].plot(self.noisyX)
        # ax[0].set_title("Estimation Error X")
        # ax[1].plot(np.array(self.trajectoryY) - np.array(self.GTy))
        # # ax[1].plot(self.noisyY)
        # ax[1].set_title("Estimation Error Y")
        # ax[2].plot(np.array(self.trajectoryTh) - np.array(self.GTth))
        # ax[2].set_title("Estimation Error Theta")

        # # ----- plot estimated and gt distance to the goal
        # fig, ax = plt.subplots()
        # ax.plot(self.dt, self.estDistToGoal, label='Estimated')
        # ax.plot(self.dt, self.gtDistToGoal, label='Ground Truth')
        # ax.set_xlabel('time step')
        # ax.set_ylabel('Distance to Goal')
        # ax.legend()

        # show all those plots
        plt.show()
    

    def AppendNoisyX(self, x):
        self.noisyX.append(x)
    def AppendNoisyY(self, y):
        self.noisyY.append(y)

    def AppendGroundX(self, x):
        self.GTx.append(x)
    def AppendGroundY(self, y):
        self.GTy.append(y)
    def AppendGroundTh(self, th):
        self.GTth.append(th)

    def AppendX(self, x):
        self.trajectoryX.append(x)
    def AppendY(self, y):
        self.trajectoryY.append(y)
    def AppendTh(self, th):
        self.trajectoryTh.append(th)
    # def CompTrajStatistics(self):
    #     """
    #     Should ground truth be a baseline control without noise or should it
    #     be a trajectory formed by the velocities in the experiment?

    #     What question do I want to answer? Is the UGV controllable under specific
    #     noise conditions?

    #     I should do it based on a baseline because the velocity ground truth does
    #     not tell us how the Kalman filter performs without noise.

    #     1) Maximum deviation from baseline
    #     2) Maximum deviation from the goal (least norm distance?)
    #     3) Cross-correlation between baseline and noisy trajectory
    #     4)  
    #     """

class DifferentialRobot(Robot):
    """
    @param x [meters] initial x position
    @param y [meters] initial y position
    @param theta [radians] initial bearing
    @param linearization point (unused)
    @param kp Proportional gain 
    @param lps_noise_actual [nanoseconds] actual tdoa measurement noise
    @param lps_noise_pred [nanoseconds] predicted tdoa measurement noise

    # TODO Propogate these new variables throughout
    @param encoder_noise_mean
    @param encoder_nosie_stdev
    """
    def __init__(self, x=0, y=0, theta=0, l=0.065, 
                kp = 1, lps_noise_actual=0.15*(10**(-9)),
                lps_std=0.15*(10**(-9)),
                encoder_on=True,
                accel_on=False,
                gyro_on=True,
                tdoa_on=False,
                controller_type = 'lqr'):
        super().__init__(x,y,theta,kp)
        self.l = l
        self.lps_std = lps_std
        self.lps_noise_actual = lps_noise_actual
        self.estimator = EKF(x,y,theta)
        self.encoder = Encoder()
        self.gyroscope = Gyro()
        self.accelerometer = Accelerometer(0.025, 0.025) # TODO: get better noise values
        self.v = 0
        self.w = 0
        self.noisy_v = 0
        self.noisy_w = 0

        self.encoder_on = encoder_on
        self.accel_on = accel_on
        self.gyro_on = gyro_on
        self.tdoa_on = tdoa_on
        self.controller_type = controller_type # lqr, proportional, or None

    def ForwardKinematics(self, v, w, dt):
            

        if(abs(w) > 0.0001):
            self.x = self.x - (v/w) * np.sin(self.theta) + (v/w)*np.sin(self.theta + w*dt)
            self.y = self.y + (v/w) * np.cos(self.theta) - (v/w)*np.cos(self.theta + w*dt)
            self.theta = self.theta + w*dt
        else:
            self.x = self.x + v*np.cos(self.theta) * dt
            self.y = self.y + v*np.sin(self.theta) * dt

        if((self.theta) > 2*np.pi):
            self.theta = self.theta - 2*np.pi
        elif((self.theta) < 2*np.pi):
            self.theta = self.theta + 2*np.pi

    def AddNoise(self, xmean, xstd, ymean, ystd):
        noiseX = (np.random.default_rng().normal(float(xmean), xstd))
        noiseY = (np.random.default_rng().normal(float(ymean), ystd))
        x = self.x + noiseX
        y = self.y + noiseY
        return (x, y)

    def PControl(self, set_x, set_y, dt, iter, in_setup):
        # simulate robot movement
        self.ForwardKinematics(self.noisy_v,self.noisy_w,dt) # Robot kinematics run all the time.
        
        # update predction with sensor readings
        self.UpdateSensors(dt, iter)

        # the actual control part
        if(iter % CTRLR_UPDATE_DIVISOR == 0): # Update rate is 20 ms * 5 = 100 ms = 10 Hz
            dt = dt * CTRLR_UPDATE_DIVISOR

            self.Estimate(self.v, self.w, dt, in_setup)

            # log data
            self.AppendGroundX(self.x)
            self.AppendGroundY(self.y)
            self.AppendGroundTh(self.theta)
            self.gtDistToGoal.append(np.sqrt((set_x-self.x)**2 + (set_y-self.y)**2))
            self.estDistToGoal.append(float(np.sqrt((set_x-self.estimator.state[0])**2 + (set_y-self.estimator.state[1])**2)))
            self.dt.append(iter)

            # calculate desired velocites to get to goal
            vx = (set_x - self.estimator.get_state()[0]) * self.Kp
            vy = (set_y - self.estimator.get_state()[1]) * self.Kp

            # calculate required v and w for those velocities
            self.v = np.cos(-self.theta) * vx - np.sin(-self.theta) * vy
            self.w = (np.sin(-self.theta) * vx + np.cos(-self.theta) * vy) / self.l

            # dont go over the max speed
            if(abs(self.v) > V_MAX):
                self.v = (self.v/abs(self.v)) * V_MAX
            if(abs(self.w) > W_MAX):
                self.w = (self.w/abs(self.w)) * W_MAX # Cap w at 0.5 rad/s

            # add in movement noise for the forward kinematics
            self.AddMovementNoise()

        return False

    def LQRControl(self, set_x, set_y, dt, iter, in_setup):
        # simulate robot movement
        self.ForwardKinematics(self.noisy_v,self.noisy_w,dt) # Robot kinematics run all the time.

        # update predction with sensor readings
        self.UpdateSensors(dt, iter)

        # the actual control part
        if (iter % CTRLR_UPDATE_DIVISOR == 0):  # Update rate is 20 ms * 5 = 100 ms = 10 Hz
            dt = dt * CTRLR_UPDATE_DIVISOR

            # run kalamn predict step
            self.Estimate(self.v, self.w, dt, in_setup)
            self.estimator.check_covariance()

            # log data
            self.AppendGroundX(self.x)
            self.AppendGroundY(self.y)
            self.AppendGroundTh(self.theta)
            self.gtDistToGoal.append(np.sqrt((set_x-self.x)**2 + (set_y-self.y)**2))
            self.estDistToGoal.append(float(np.sqrt((set_x-self.estimator.state[0])**2 + (set_y-self.estimator.state[1])**2)))
            self.dt.append(iter)

            # Actual state
            # Our robot starts out at the origin (x=0 meters, y=0 meters), and
            # the yaw angle is 0 radians.
            x1 = self.estimator.state[0]
            y1 = self.estimator.state[1]
            th1 = self.estimator.state[2]
            actual_state_x = np.array([float(x1), float(y1), float(th1)])
            #print(f'Current State = {actual_state_x}')

            # Desired state [x,y,yaw angle]
            # [meters, meters, radians]
            desired_state_xf = np.array([set_x, set_y, np.pi / 4])

            # A matrix
            # Expresses how the state of the system [x,y,yaw] changes
            # from t-1 to t when no control command is executed.
            # For this case, A is the identity matrix.
            A = np.array([[1.0, 0, 0],
                          [0, 1.0, 0],
                          [0, 0, 1.0]])

            # R matrix
            # The control input cost matrix
            # Experiment with different R matrices
            # This matrix penalizes actuator effort (i.e. rotation of the
            # motors on the wheels that drive the linear velocity and angular velocity).
            # We can target control inputs where we want low actuator effort
            # by making the corresponding value of R large.
            R = np.array([[.01, 0],  # Penalty for linear velocity effort
                          [0, .01]])  # Penalty for angular velocity effort

            # Q matrix
            # The state cost matrix.
            # Q helps us weigh the relative importance of each state in the
            # state vector (X, Y, YAW ANGLE).
            # Q enables us to target states where we want low error by making the
            # corresponding value of Q large.
            Q = np.array([[1, 0, 0],  # Penalize X position error
                          [0, 1, 0],  # Penalize Y position error
                          [0, 0, 1]])  # Penalize YAW ANGLE heading error

            state_error = actual_state_x - desired_state_xf
            state_error_magnitude = np.linalg.norm(state_error)
            #print(f'State Error Magnitude = {state_error_magnitude}')

            B = LQR.getB(float(th1), dt)

            # LQR returns the optimal control input
            u, k = LQR.lqr(actual_state_x,
                           desired_state_xf,
                           Q, R, A, B, dt)

            self.v = float(u[0])
            self.w = float(u[1])

            # max speeds
            if (abs(self.w) / dt > W_MAX):
                self.w = (self.w / abs(self.w)) * W_MAX  # Cap w at 0.5 rad/s
            if (abs(self.v) / dt > V_MAX):
                self.v = (self.v / abs(self.v)) * V_MAX

            # add in movement noise for the forward kinematics
            self.AddMovementNoise()

        return False

    def NoControl(self, set_x, set_y, dt, iter, in_setup):
        # simulate robot movement
        self.ForwardKinematics(self.noisy_v,self.noisy_w,dt) # Robot kinematics run all the time.
        
        # update prediction with sensor readings
        self.UpdateSensors(dt, iter)

        if(iter % CTRLR_UPDATE_DIVISOR == 0): # Update rate is 20 ms * 5 = 100 ms = 10 Hz
            dt = dt * CTRLR_UPDATE_DIVISOR

            self.Estimate(self.v, self.w, dt, in_setup)

            # log data
            self.AppendGroundX(self.x)
            self.AppendGroundY(self.y)
            self.AppendGroundTh(self.theta)
            self.gtDistToGoal.append(np.sqrt((set_x-self.x)**2 + (set_y-self.y)**2))
            self.estDistToGoal.append(float(np.sqrt((set_x-self.estimator.state[0])**2 + (set_y-self.estimator.state[1])**2)))
            self.dt.append(iter)

            # just drive forward
            self.v = .08
            self.w = 0

            # dont go over the max speed
            if(abs(self.v) > V_MAX):
                self.v = (self.v/abs(self.v)) * V_MAX
            if(abs(self.w) > W_MAX):
                self.w = (self.w/abs(self.w)) * W_MAX # Cap w at 0.5 rad/s

            # add in movement noise for the forward kinematics
            self.AddMovementNoise()

        return False

    # give the robot a forward and rotational velocity bias, with some random noise
    # simulates physical deformities in the robot that would affect the dynamics (bad motor, unaligned wheels, etc)
    def AddMovementNoise(self):
        if abs(self.v) > 0:
            self.noisy_v = self.v + np.random.normal(0.01, self.v_std)

        if abs(self.v) > 0 or abs(self.w) > 0:
            self.noisy_w = self.w + np.random.normal(-.02, self.w_std)

    # choose the correct controller
    def RunController(self,set_x,set_y,dt,iter, in_setup):
        if self.controller_type == 'lqr':
            return self.LQRControl(set_x, set_y, dt,iter, in_setup)
        elif self.controller_type == 'proportional':
            return self.PControl(set_x, set_y, dt,iter, in_setup)
        elif self.controller_type == 'no_controller':
            return self.NoControl(set_x, set_y, dt,iter, in_setup)

    # perform the update step of the kalman filter with each sensor 
    # if is is that sensors time (based on their update rate)
    def UpdateSensors(self, dt, iter):
        # ----- 'Read' Encoder Values -----
        if (iter % 12.5 == 0):  # 4 Hz Update rate with 20 ms step size
            # TODO: Replace v and w with a sincle state variable to
            # TODO: to match the proposed interface for the robot

            if self.encoder_on:
                self.encoder.update_sensor(self.noisy_v, self.noisy_w, dt * 12.5)  # compute the ticks

                # update step for encoder
                [rticks, lticks] = self.encoder.get_measurement()
                self.encoder.reset_ticks()
                self.estimator.update_encoders([rticks, lticks, 18], dt*12.5)
                self.estimator.check_covariance()

        # ----- 'Read' IMU Values -----
        if(iter % 5 == 0): # 10 Hz Update rate with 20 ms step size
            if self.gyro_on:
                self.gyroscope.update_sensor(self.noisy_w)

                # update step for gyro
                w_measured = self.gyroscope.get_measurement()
                self.estimator.update_gyroscope(w_measured, dt*5)
                self.estimator.check_covariance()

            if self.accel_on:
                self.accelerometer.update_sensor(dt*5, self.noisy_v)

                # update step for accel
                v_measured = self.accelerometer.get_measurement()
                self.estimator.update_accelerometer(v_measured, dt*5)
                self.estimator.check_covariance()

            if self.tdoa_on:
                # tdoa
                self.estimator.update_tdoa_scalar(pos_sys.GetAnchorsTdoa2((self.x,self.y,0), self.lps_noise_actual))
                self.estimator.check_covariance()

    def Estimate(self, v, w, dt, in_setup):

        if not in_setup:
            self.AppendX(self.estimator.get_state()[0])
            self.AppendY(self.estimator.get_state()[1])
            self.AppendTh(self.estimator.get_state()[2])

        self.estimator.predict(np.matrix([[v],[w]]), dt)

                # #
        # if abs(self.estimator.get_state()[0]) > 15 or abs(self.estimator.get_state()[1]) > 15:
        #     self.estimator.reset_estimator() 
        # print("After Update: {}".format(self.estimator.get_state()))
    
class Simulator:
    def __init__(self, model: Robot, timestep=1, duration=100):
        self.timestep = timestep
        self.duration = duration
        self.model = model
        self.setpoint = None

        self.in_setup = True

    def run(self, set_x,set_y, plot=True):
        done = False
        stop_ind = 0
        self.setpoint = (set_x, set_y)
        self.in_setup = True
        # logging.info("Initializing extended Kalman filter...")
        # for i in range(1000):
        #     CURRENT_TIME = i * self.timestep
        #     printProgressBar(i, 1000, "Progress")
        #     self.model.Estimate(self.model.x,self.model.y,self.timestep,self.in_setup)

        self.in_setup = False

        logging.info("Running simulation...")
        for i in range(self.duration):
            printProgressBar(i,self.duration,"Progress: ")
            # CURRENT_TIME = i * self.timestep
            if(self.model.RunController(set_x,set_y,self.timestep, i, self.in_setup)):
                if not done:
                    stop_ind = i
                    done = True
        print()
        self.model.SetStoptime = stop_ind * self.timestep
        #self.compute_controls_stats_output()

        if plot:
            self.model.PlotTrajectory()

    def find_percent_os(self, df):
        perc_x = (df['X'].max()-self.setpoint[0]) / self.setpoint[0]
        perc_y = (df['Y'].max() - self.setpoint[1]) / self.setpoint[1]
        return (perc_x, perc_y)

    def find_risetime(self, df):
        ten_perc_x = 0 
        ninty_perc_x = 0 
        ten_perc_y = 0 
        ninty_perc_y = 0 
        # print("{}".format(self.setpoint))
        for i in range(1,df.shape[0]):
            if df['X'][i-1] < self.setpoint[0] * 0.1 and df['X'][i] >= self.setpoint[0] * 0.1:
                ten_perc_x = i
            if df['Y'][i-1] < self.setpoint[1] * 0.1 and df['Y'][i] >= self.setpoint[1] * 0.1:
                ten_perc_y = i
            if df['X'][i-1] < self.setpoint[0] * 0.9 and df['X'][i] >= self.setpoint[0] * 0.9:
                ninty_perc_x = i
            if df['Y'][i-1] < self.setpoint[1] * 0.9 and df['Y'][i] >= self.setpoint[1] * 0.9:
                ninty_perc_y = i
        # print("{},{}".format(ninty_perc_x, ten_perc_x))
        rise_x_ticks = ninty_perc_x - ten_perc_x
        rise_y_ticks = ninty_perc_y - ten_perc_y
        rise_time_x = rise_x_ticks * SEC_PER_TICK
        rise_time_y = rise_y_ticks * SEC_PER_TICK
        return (rise_time_x, rise_time_y)

    def find_two_percent_settling_time(self, df):
        # for i in range(self.) 
        pass

    def compute_controls_stats_output(self):
        traj_df = pd.DataFrame([self.model.trajectoryX[:1000], self.model.trajectoryY[:1000]])
        traj_df = traj_df.transpose()
        traj_df.columns = ['X', 'Y']
        (rise_time_x, rise_time_y) = self.find_risetime(traj_df)
        (perc_x, perc_y) = self.find_percent_os(traj_df)
        pth = "./trial-{}-{}".format(self.model.lps_noise_actual, self.model.lps_std)
        try:
            os.mkdir(pth)
        except Exception as e:
            pass
        with open(pth + "/control_statistics-{}-{}.csv".format(self.model.lps_noise_actual, self.model.lps_std), "w+") as datafile:
            datafile.write("percentOSx, percentOSy, risetimex, risetimey\n")
            datafile.write("{},{},{},{}\n".format(perc_x, perc_y, rise_time_x, rise_time_y)) 
        
        conf = {
            "lps_actual_noise": self.model.lps_noise_actual,
            "lps_pred_noise": self.model.lps_std,
            "setpointx" : self.setpoint[0],
            "setpointy" : self.setpoint[1],
            "timesteps" : self.duration,
            "timescale" : self.timestep,
            "controller_update_div" : CTRLR_UPDATE_DIVISOR
        }
        json_obj = json.dumps(conf, indent = 4)
        with open(pth + "/trial.config", "w+") as datafile:
            datafile.write(json_obj)
        

        df = pd.DataFrame([self.model.trajectoryX,self.model.trajectoryY,self.model.GTx, self.model.GTy])
        df = df.transpose()
        df.columns = ['X', 'Y', 'GTX', 'GTY']
        df.to_csv(pth + "/raw_data.csv")
        
# def begin_sim(lps_actual_noise, lps_pred_noise):
#     rob = DifferentialRobot(0,0,0,0.065,10, lps_actual_noise, lps_pred_noise)
#     sim = Simulator(rob, 0.02, 5000)
#     sim.run(1,1)
#     return "Sim Done"

def run_trials(num_trials=2):
    results = pd.DataFrame()
    results_long = pd.DataFrame()

    # trials configs
    # (encoder, gyro, accel, controller)
    configs = [
        # only encoder
        (True, False, False, 'no_controller'),
        (True, False, False, 'lqr'),
        (True, False, False, 'proportional'),

        # only imu (gyro and accel must be used together)
        (False, True, True, 'no_controller'),
        (False, True, True, 'lqr'),
        (False, True, True, 'proportional'),

        # all sensors
        (True, True, True, 'no_controller'),
        (True, True, True, 'lqr'),
        (True, True, True, 'proportional'),
    ]

    for encoder, gyro, accel, controller in configs:
        for trial_num in range(num_trials):
            print(f"encoder_on: {encoder} | gyro_on: {gyro} | accel_on: {accel} | controller_type: {controller} | trial: {trial_num}")
            #pr_noise =  np.linspace(0,1,21) * 10**(-9)
            lps_actual_noise = 0.17 * (10**(-9))

            rob = DifferentialRobot(0,0,0,0.065,10, lps_actual_noise, lps_actual_noise,
                encoder_on=encoder,
                gyro_on=gyro,
                accel_on=accel,
                controller_type=controller)

            sim = Simulator(rob, 0.02, 5000)
            sim.run(3,3, plot=False)

            # save the results
            x_error = np.array(rob.trajectoryX) - np.array(rob.GTx)
            y_error = np.array(rob.trajectoryY) - np.array(rob.GTy)
            th_error = np.array(rob.trajectoryTh) - np.array(rob.GTth)

            final_gt_dist_to_goal = rob.gtDistToGoal[-1]
            final_est_dist_to_goal = rob.estDistToGoal[-1]

            #short results
            results = results.append(dict(
                encoder_on=encoder,
                gyro_on=gyro,
                accel_on=accel,
                controller=controller,
                trial=trial_num,
                x_error=x_error,
                y_error=y_error,
                th_error=th_error,
                final_gt_dist_to_goal=final_gt_dist_to_goal,
                final_est_dist_to_goal=final_est_dist_to_goal), ignore_index=True)

            # long results
            for i in range(len(rob.dt)):
                results_long = results_long.append(dict(
                    encoder_on=encoder,
                    gyro_on=gyro,
                    accel_on=accel,
                    controller=controller,
                    trial=trial_num,
                    dt=rob.dt[i],
                    x_error=x_error[i],
                    y_error=y_error[i],
                    th_error=th_error[i],
                    gt_dist_to_goal=rob.gtDistToGoal[i],
                    est_dist_to_goal=rob.gtDistToGoal[i]), ignore_index=True)

    results_long.to_csv(f"results_{datetime.now().strftime('%Y-%m-%d %H-%M-%S')}.csv")

    process_trials_data(results_long)
    #print(results)

def process_trials_data(df):
    # process data
    p_df = pd.DataFrame(columns=[
        'gt_dist_to_goal_mean','est_dst_to_goal_mean','x_error_mean','y_error_mean',
        'gt_dist_to_goal_std','est_dst_to_goal_std','x_error_std','y_error_std'])

    # group by config type
    p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).agg({
                                                                                    'gt_dist_to_goal': ['mean','std'],
                                                                                    'est_dist_to_goal': ['mean','std'],
                                                                                    'x_error': ['mean','std'],
                                                                                    'y_error': ['mean','std'],
                                                                                    }).reset_index()
    # p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).est_dist_to_goal.mean().agg({'est_dist_to_goal': ['mean']}).reset_index()
    # p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).x_error.mean().agg({'x_error': ['mean']}).reset_index()
    # p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).y_error.mean().agg({'y_error': ['mean']}).reset_index()

    # p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).agg({'gt_dist_to_goal': ['std']}).reset_index()
    # p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).agg({'est_dist_to_goal': ['std']}).reset_index()
    # p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).agg({'x_error': ['std']}).reset_index()
    # p_df = df.groupby(by=['encoder_on','gyro_on','accel_on','controller', 'dt']).agg({'y_error': ['std']}).reset_index()

    p_df.columns = ['encoder_on', 'gyro_on', 'accel_on', 'controller', 'dt',
        'gt_dist_to_goal_mean','est_dst_to_goal_mean','x_error_mean','y_error_mean',
        'gt_dist_to_goal_std','est_dst_to_goal_std','x_error_std','y_error_std']
    

    return p_df

def plot_trials(df):
    fig = go.Figure()
    # no controllers
    d = df.loc[(df['encoder_on'] == 0) & (df['gyro_on'] == 1) & (df['accel_on'] == 1) &  (df['controller'] == 'no_controller')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='IMU with no controller', line=dict(dash='dot', color='red')))
   
    d = df.loc[(df['encoder_on'] == 1) & (df['gyro_on'] == 0) & (df['accel_on'] == 0) &  (df['controller'] == 'no_controller')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='Encoder with no controller', line=dict(dash='dashdot', color='red')))

    d = df.loc[(df['encoder_on'] == 1) & (df['gyro_on'] == 1) & (df['accel_on'] == 1) &  (df['controller'] == 'no_controller')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='All Sensors with no controller', line=dict(dash='dash', color='red')))

    # proportionals
    d = df.loc[(df['encoder_on'] == 0) & (df['gyro_on'] == 1) & (df['accel_on'] == 1) &  (df['controller'] == 'proportional')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='IMU with PControl', line=dict(dash='dot', color='blue')))

    d = df.loc[(df['encoder_on'] == 1) & (df['gyro_on'] == 0) & (df['accel_on'] == 0) &  (df['controller'] == 'proportional')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='Encoders with PControl', line=dict(dash='dashdot', color='blue')))

    d = df.loc[(df['encoder_on'] == 1) & (df['gyro_on'] == 1) & (df['accel_on'] == 1) &  (df['controller'] == 'proportional')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='All Sensors with PControl', line=dict(dash='dash', color='blue')))

    # lqrs
    d = df.loc[(df['encoder_on'] == 0) & (df['gyro_on'] == 1) & (df['accel_on'] == 1) &  (df['controller'] == 'lqr')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='IMU with LQR', line=dict(dash='dot', color='green')))

    d = df.loc[(df['encoder_on'] == 1) & (df['gyro_on'] == 0) & (df['accel_on'] == 0) &  (df['controller'] == 'lqr')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='Encoders with LQR', line=dict(dash='dashdot', color='green')))

    d = df.loc[(df['encoder_on'] == 1) & (df['gyro_on'] == 1) & (df['accel_on'] == 1) &  (df['controller'] == 'lqr')]
    fig.add_trace(go.Scatter(x=d.dt, y=d.gt_dist_to_goal_mean, name='All Sensors with LQR', line=dict(dash='dash', color='green')))

    fig.update_layout(
        #title="Plot Title",
        xaxis_title="Time (ms)",
        yaxis_title="Distance to Goal (m)"
    )


    fig.show()

if __name__ == '__main__':
    df = pd.read_csv('results_2021-06-06 04-11-07.csv')

    p_df = process_trials_data(df)
    p_df = p_df[np.mod(np.arange(p_df.index.size),4)!=0]

    plot_trials(p_df)

    # -----

    #run_trials()

    # -----

    # format = "%(asctime)s: %(message)s"
    # logging.basicConfig(format=format, level=logging.INFO,
    #                     datefmt="%H:%M:%S")
    # # ac_noise = np.linspace(0,1,21)
    # pr_noise =  np.linspace(0,1,21) * 10**(-9)
    # lps_actual_noise = 0.17 * (10**(-9))

    # rob = DifferentialRobot(0,0,0,0.065,10, lps_actual_noise, lps_actual_noise,
    #     encoder_on=True,
    #     gyro_on=True,
    #     accel_on=True,
    #     #controller_type='lqr',
    #     #controller_type='proportional',
    #     controller_type=None
    #     )

    # sim = Simulator(rob, 0.02, 5000)
    # sim.run(3,3)




