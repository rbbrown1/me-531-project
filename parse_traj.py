import pandas as pd
import numpy as np
import matplotlib as mpt
import matplotlib.pyplot as plt
import os
import re

setpoint = (1, 1)
SEC_PER_TICK = 0.02

def find_risetime(df):
        ten_perc_x = 0 
        ninty_perc_x = 0 
        ten_perc_y = 0 
        ninty_perc_y = 0 

        for i in range(1,df.shape[0]):
            # print(setpoint[0] * 0.1)
            if df['X'][i-1] < setpoint[0] * 0.1 and df['X'][i] >= setpoint[0] * 0.1:
                ten_perc_x = i
            if df['Y'][i-1] < setpoint[1] * 0.1 and df['Y'][i] >= setpoint[1] * 0.1:
                ten_perc_y = i
            if df['X'][i-1] < setpoint[0] * 0.9 and df['X'][i] >= setpoint[0] * 0.9:
                ninty_perc_x = i
            if df['Y'][i-1] < setpoint[1] * 0.9 and df['Y'][i] >= setpoint[1] * 0.9:
                ninty_perc_y = i
        
        rise_x_ticks = ninty_perc_x - ten_perc_x
        rise_y_ticks = ninty_perc_y - ten_perc_y
        rise_time_x = rise_x_ticks * SEC_PER_TICK
        rise_time_y = rise_y_ticks * SEC_PER_TICK
        return (rise_time_x, rise_time_y)

def find_percent_os(df):
    perc_x = (df['X'].max()- setpoint[0]) / setpoint[0]
    perc_y = (df['Y'].max() - setpoint[1]) / setpoint[1]
    return (perc_x, perc_y)

trialregex = re.compile(r'trial(.*)')

if __name__ == '__main__':
    dirs = [dI for dI in os.listdir('.') if os.path.isdir(os.path.join('.',dI))]
    dirs = [di for di in dirs if 'trial' in di]
    for di in dirs:
        trial_num = trialregex.findall(di)
        print("Opening {}".format('./' + di + '/raw_data.csv'))
        df = pd.read_csv('./' + di + '/raw_data.csv')
        df = df[1000:80000]
        df = df.reset_index()
        # print(df)
        (x_rise, y_rise) = find_risetime(df) 
        (perc_x, perc_y) = find_percent_os(df)
        print("{}, {}".format(x_rise, y_rise))
        print("{}, {}".format(perc_x, perc_y))
        print()