import numpy as np
from enum import Enum
import logging

from numpy.lib.shape_base import dstack

""" Suppose measurement matrix (N x L):

M = |   x1  y1  |
    |   x2  y2  |
    |   x3  y3  |
    |   x4  y4  |

    The deviation matrix L is:
    L = M -[1] M * N^-1

    The covariance matrix is:
        R = LL^T
    
    It follows that if our expected error on 
    TDOA data is +/- 10 cm (0.1 m), then the 
    deviation is 0.1, and the covariance matrix
    is the identity (because the variables are
    decoupled) multiplied by 0.01.
"""
MEASUREMENT_NOISE_STD_TDOA = 0.5 
# CF sets this to 0.15

proc_cov_tdoa = np.matrix([[MEASUREMENT_NOISE_STD_TDOA ** 2, 0], 
                           [0, MEASUREMENT_NOISE_STD_TDOA ** 2]])

class KFstate_I(Enum):
    X = 0
    Y = 1
    THETA = 2
    V = 3
    W = 4

class steering(Enum):
    V = 0
    OMEGA = 1

class EKF:
    """A class that implements the extended kalman filter.
    NOTE: Units are in meters, radians and seconds unless 
    otherwise specified. 
    
    Current sensor measurements supported:
        TDoA (UWB)
    """
    def __init__(self, x, y, theta):
        self.log = logging.getLogger(__name__)


        # //Initialize filter with high uncertainty of position
        self.init_proc_cov = 0.01
        self.pred_cov = np.matrix([[self.init_proc_cov ** 2, 0, 0, 0, 0],
                                   [0, self.init_proc_cov ** 2, 0, 0, 0],
                                   [0, 0, self.init_proc_cov ** 2, 0, 0],
                                   [0, 0, 0, self.init_proc_cov ** 2, 0],
                                   [0, 0, 0, 0, self.init_proc_cov ** 2]])

        self.proc_cov = np.matrix([[self.init_proc_cov ** 2, 0, 0, 0, 0],
                                   [0, self.init_proc_cov ** 2, 0, 0, 0],
                                   [0, 0, self.init_proc_cov ** 2, 0, 0],
                                   [0, 0, 0, self.init_proc_cov ** 2, 0],
                                   [0, 0, 0, 0, self.init_proc_cov ** 2]])

        # Begin at (x,y) = (3,0) and facing parallel to x-axis, Zero velocity
        self.state = np.matrix([[x],[y],[theta], [0],[0]])

        # Steering matrix
        self.U = np.matrix([[0],[0]])

        # Function dictionary for the correction step
        # TODO: wont work until each funciton has the same inputs
        self.correction_functions = {
            "tdoa": self.update_tdoa,
            "encoder": self.update_encoders,
            "gyroscope": self.update_gyroscope,
            "accelerometer": self.update_accelerometer,
        }


    def print_state(self):
        """ A function to print the state of the system
        """
        print("X: {}, Y: {}, Theta: {}".format(self.state[KFstate_I.X.value,0],self.state[KFstate_I.Y.value,0],self.state[KFstate_I.THETA.value,0]))

    def print_covariance(self):
        """ A function to print the predicted covariance of the system
        """
        print("Covariance:\n{}".format(self.pred_cov))

    # check the covariance for erroneous values
    def check_covariance(self):
        size_y, _ = self.proc_cov.shape

        for i in range(size_y):
            if abs(self.proc_cov[i,i]) < 0.000000001:
                self.proc_cov[i,i] = 0.000000001

    def get_b(self, theta, dt):
        """
        Calculates and returns the B matrix
        3x2 matix -> number of states x number of control inputs
        The control inputs are the forward speed and the
        rotation rate around the z axis from the x-axis in the 
        counterclockwise direction.
        [v, w]
        Expresses how the state of the system [x,y,yaw] changes
        from t-1 to t due to the control commands (i.e. control
        input).
        :param theta: The theta (absolute rotation angle around the z axis) in rad 
        :param dt: The change in time from time step t-1 to t in sec
            """
        B = np.array([[np.cos(theta)*dt, 0],
                        [np.sin(theta)*dt, 0],
                        [0, dt]])
        return B

    def predict(self, U, dt=1.0):
        """ The prediction step of the kalman filter. Given
        the steering matrix U = [V, OMEGA]^T, the current state
        self.state, and the dt that has occured (default 1 second),
        we can determine the new state through the Jacobian of the
        equations of motion.
        """
        # Predict the linearized change in the state over the time step
        u = np.matrix([[self.state[3,0]], [self.state[4,0]]])
        #print(u)
        dState = self.differential_model(u, dt)
        motion_jacobian = self.differential_jacobian(u, dt)

        dState = np.matmul(motion_jacobian, dState)

        # self.state[3] = U[0]
        # self.state[4] = U[1]

        # Add to previous state to get new state
        self.state = np.add(self.state, dState)

        # add motion controls to state

        # Bounding theta to [0, 2pi]
        # if self.state[KFstate_I.THETA.value, 0] > (2 * np.pi):
        #     self.state[KFstate_I.THETA.value, 0] -= (2*np.pi)
        # elif self.state[KFstate_I.THETA.value, 0] < 0:
        #     self.state[KFstate_I.THETA.value, 0] += (2 * np.pi)

        """ Update the process covariance 
        Pk = dA/du * Pk-1 * (dA/du)^T + Qk (Q = noise/error)
        https://doi.org/10.1155/2013/249594  
        """
        jTimesPk = np.matmul(motion_jacobian, self.pred_cov)
        cov_update = np.matmul(jTimesPk, motion_jacobian.T)

        prediction_noise = .05
        Qk = np.matrix([[prediction_noise, 0, 0, 0, 0],
                        [0, prediction_noise, 0, 0, 0],
                        [0, 0, prediction_noise, 0, 0],
                        [0, 0, 0, prediction_noise, 0],
                        [0, 0, 0, 0, prediction_noise]])

        self.proc_cov = np.add(cov_update, Qk)

# =======
        # """ Update the prediction covariance 
        #     Pk = dA/du * Pk-1 * (dA/du)^T + Qk
        #                               error ^
        # """
#         """ Update the process covariance 
#         Pk = dA/du * Pk-1 * (dA/du)^T + Qk (Q = noise/error)
#         """
#         jTimesPk = np.matmul(motion_jacobian, self.proc_cov)
#         # print("{}, {}".format(jTimesU.shape, motion_jacobian.))
#         cov_update = np.matmul(jTimesPk, motion_jacobian.T)
        # tempMat1 = np.matmul(motion_jacobian, self.pred_cov)
        # cov_update = np.matmul(tempMat1, motion_jacobian.T)
        # self.pred_cov = np.add(self.pred_cov, cov_update) 


    def correction_step(self, data):
        """ Use function pointers to make this extensible
        to multiple different types of measuerements. Add
        the update function of the measurement to the
        correction functions dictionary.
        """
        for fx in self.correction_functions:
            fx(data)

    def update_position(self, pos_meas):
        meas_x = pos_meas[KFstate_I.X.value]
        meas_y = pos_meas[KFstate_I.Y.value]
        # meas_z = pos_meas[KFstate_I.Z.value]
        
        measured_position = np.matrix([[meas_x],[meas_y],[0]])
        # measured_position = np.matrix([[meas_x],[meas_y],[meas_z],[0]])
        innovation = np.subtract(measured_position, self.state)

        Rk = np.matrix([[MEASUREMENT_NOISE_STD_TDOA ** 2,0,0],\
                        [0, MEASUREMENT_NOISE_STD_TDOA**2, 0],\
                        [0, 0, MEASUREMENT_NOISE_STD_TDOA**2]])
        SkRk = np.add(self.pred_cov, Rk)
        kalmanGain = np.matmul(self.pred_cov, SkRk.I)
        self.state = np.add(self.state, np.matmul(kalmanGain,innovation))
        
        # print("State: {}".format(self.state))
        # print("Gain: {}".format(kalmanGain))

        khi = np.subtract(kalmanGain, np.identity(3))
        self.pred_cov = np.matmul(khi, self.pred_cov)
        self.pred_cov = np.matmul(self.pred_cov, khi.T)

        noise = np.matmul(kalmanGain, Rk)
        noise = np.matmul(noise, kalmanGain.T)

        self.pred_cov = np.add(self.pred_cov, noise)
        # print("Cov: {}".format(self.pred_cov))
        # for i in self.pred_cov.shape[0]:
        #     for j in self.pred_cov.shape[1]:

                
    def reset_estimator(self):
        # self.state = np.matrix([[0],[0],[0]])
        # self.pred_cov = np.matrix([[self.init_proc_cov_x ** 2, 0 , 0],
        #                            [0, self.init_proc_cov_y ** 2, 0],
        #                            [0, 0,  self.init_proc_cov_th ** 2]])
        self.pred_cov = np.matrix([[self.init_proc_cov ** 2, 0, 0, 0, 0],
                                   [0, self.init_proc_cov ** 2, 0, 0, 0],
                                   [0, 0, self.init_proc_cov ** 2, 0, 0],
                                   [0, 0, 0, self.init_proc_cov ** 2, 0],
                                   [0, 0, 0, 0, self.init_proc_cov ** 2]])

        # Begin at (x,y) = (0,0) and facing parallel to x-axis, Zero velocity
        self.state = np.matrix([[0],[0],[0], [0],[0]])

    def update_tdoa_scalar(self, tdoa):
        # print("tdoa: {}".format(tdoa))
        # Anchor 1 position
        x1 = tdoa[1][0]
        y1 = tdoa[1][1]
        z1 = tdoa[1][2]

        # Anchor 0 position (not actual index, just in this measurement)
        x0 = tdoa[0][0]
        y0 = tdoa[0][1]
        z0 = tdoa[0][2]

        # differences
        dx0 =  self.state.item(KFstate_I.X.value) - x0
        dy0 =  self.state.item(KFstate_I.Y.value) - y0 
        dz0 =  0 - z0 #self.state[KFstate_I.Z.value][0]
        # dz0 = 0 - z0
        # print("Diff0: {}, {}, {}".format(dx0, dy0,dz0))
        dx1 = self.state.item(KFstate_I.X.value) - x1
        dy1 = self.state.item(KFstate_I.Y.value) - y1
        dz1 = 0 - z1 #self.state[KFstate_I.Z.value][0]
        # print("Diff1: {}, {}, {}".format(dx1, dy1,dz1))
        # dz1 = 0 - z1
        # print("{},{},{},{}".format(type(dx0), type(dy0), type(dx1), type(dy1)))
        # print("Shape: {}".format(dx0.shape))
        # print(self.state) 
        # Calculate ultimate distance
        d1 = np.sqrt((dx1 ** 2) + (dy1 ** 2) + (dz1 ** 2))
        d0 = np.sqrt((dx0 ** 2) + (dy0 ** 2) + (dz0 ** 2))
        if(d1 == 0.0 or d0 == 0.0):
            return
        # print("d0: {}, d1: {}".format(d0, d1)) 
        # Measured distance diff
        measurement = tdoa[2][0]
        # print("Measurement: {}".format(measurement))
        # predicted distance diff
        predicted = d1 - d0
        # print("Prediction: {}".format(predicted))

        self.log.debug("Predicted: {}".format(predicted))
        self.log.debug("Measurement: {}".format(measurement))
        innovation = measurement - predicted

        # Mapping state space to observation space

        dx1DivD1 = dx1 / d1
        dx0DivD0 = dx0 / d0
        dy1DivD1 = dy1 / d1
        dy0DivD0 = dy0 / d0
        dz1DivD1 = dz1 / d1
        dz0DivD0 = dz0 / d0

        # print("Divs0: {}, {}, {}".format(dx0DivD0, dy0DivD0,dz0DivD0))
        # print("Divs1: {}, {}, {}".format(dx1DivD1, dy1DivD1,dz1DivD1))
        # self.log.debug("{},{},{},{}".format(dx1DivD1,dx0DivD0,dy1DivD1,dy0DivD0))

        diffDX = dx1DivD1 - dx0DivD0
        diffDY = dy1DivD1 - dy0DivD0
        diffDZ = dz1DivD1 - dz0DivD0
        # self.log.debug("DiffD1: {}".format(diffD1))
        # self.log.debug("DiffD0: {}".format(diffD0))

        # Third row is all zero to emulate that the z-axis has no effect
        # in the ground robots

        Htdoa = np.matrix([diffDX,diffDY, 0, 0, 0])
        # Htdoa = np.matrix([diffDX,diffDY,diffDZ, 0])
        # print("H: {}".format(Htdoa))
        # print("Covariance: {}".format(self.pred_cov))
        """ Innovation covariance Sk = Hk Pk Hk^T + Rk"""
        Rk = MEASUREMENT_NOISE_STD_TDOA ** 2
         
        innovation_cov = Rk
        """ PkHk^T """
        PHTm = np.matmul(self.pred_cov, Htdoa.transpose())
        innovation_cov += np.dot(Htdoa, PHTm)
        # print("Innovation Covariance times H: {}".format(PHTm))
        """ Kalman gain Kk = PkHkT / Sk """
        Kgain = PHTm / innovation_cov
        # print("Gain: {}".format(Kgain))
        self.log.debug(self.state)
        self.log.debug(innovation)
        self.log.debug(Kgain * innovation)
        self.state = np.add(self.state, (Kgain * innovation)) 
        """ State update """
        
        """ Covariance Update """
        khi = np.subtract(np.matmul(Kgain, Htdoa), np.identity(self.pred_cov.shape[0]))
        self.pred_cov = np.matmul(khi, self.pred_cov)
        self.pred_cov = np.matmul(self.pred_cov, khi.transpose())

        """ Add measurement noise """
        for i in range(0,self.state.shape[0]):
            v = Kgain[i,0] * Rk * Kgain[i,0]
            p = 0.5 * self.pred_cov[i,i] + 0.5 * self.pred_cov[i,i] + v
            self.pred_cov[i,i]=p

        """ Covariance off-diag should be zeros """
        for i in range(0, self.state.shape[0]):
            for j in range(0, self.state.shape[0]):
                if i != j:
                    self.pred_cov[i, j] = 0


    def update_encoders(self, ticks, dt):
        """ @fn update_encoders
            @param ticks An array of tick values: [lticks, rticks, ticksPerRev=18]
                on the wheels
            @param dt Change in time (seconds) from the previous update
        """
        wheelDiameter = 0.025
        maxVel = 1.2 * wheelDiameter * np.pi
        ugvWidth = 0.06
        minLinSpeed = 0.025

        lticks = ticks[0]
        rticks = ticks[1]
        ticksPerRev = ticks[2]

        """First update the velocities"""
        Rvel_mps = (rticks / ticksPerRev) * wheelDiameter * np.pi / dt        
        Lvel_mps = (lticks / ticksPerRev) * wheelDiameter * np.pi / dt        

        meas_w = (Rvel_mps - Lvel_mps) / ugvWidth
        meas_v = Rvel_mps - (ugvWidth / 2) * meas_w


        PROC_NOISE_VAR_V = 0.01
        PROC_NOISE_VAR_W = 0.005
        Qt = np.zeros(self.proc_cov.shape)
        Qt[KFstate_I.V.value][KFstate_I.V.value] = PROC_NOISE_VAR_V
        Qt[KFstate_I.W.value][KFstate_I.W.value] = PROC_NOISE_VAR_W

        """ Kalman Gain (@ == matrix multiplication)"""
        K = self.proc_cov @ (np.add(self.proc_cov, Qt).I)
        #print(K)

        # get the residual
        change = np.matrix([[0],
                            [0],
                            [0],
                            [meas_v - self.state.item(KFstate_I.V.value)], 
                            [meas_w - self.state.item(KFstate_I.W.value)]])  

        """ Update the Covariance """
        self.proc_cov = np.subtract(np.identity(self.proc_cov.shape[0]), K) @ self.proc_cov

        #print(change)
        """ Then update the state"""
        dState = np.matmul(K,  change)
        self.state = np.add(self.state, dState)

    def update_gyroscope(self, sensor_reading, dt):
        meas_w = sensor_reading

        PROC_NOISE_VAR_W = .005
        Qt = np.zeros(self.proc_cov.shape)
        Qt[KFstate_I.W.value][KFstate_I.W.value] = PROC_NOISE_VAR_W

        """ Kalman Gain (@ == matrix multiplication?)"""
        K = self.proc_cov @ (np.add(self.proc_cov, Qt).I)

        # get the residual
        change = np.matrix([[0],
                            [0],
                            [0],
                            [0], 
                            [meas_w - self.state.item(KFstate_I.W.value)]])

        dState = np.matmul(K,  change) 
        self.state = np.add(self.state, dState)

        """ Update the Covariance """
        self.proc_cov = np.subtract(np.identity(self.proc_cov.shape[0]), K) @ self.proc_cov

    def update_accelerometer(self, meas_v, dt):

        PROC_NOISE_VAR_V = 0.0000000003
        Qt = np.zeros(self.proc_cov.shape)
        Qt[KFstate_I.V.value][KFstate_I.V.value] = PROC_NOISE_VAR_V

        # get the residual
        change = np.matrix([[0],
                        [0],
                        [0],
                        [meas_v - self.state.item(KFstate_I.V.value)],
                        [0]])

        """ Kalman Gain (@ == matrix multiplication?)"""
        K = self.proc_cov @ (np.add(self.proc_cov, Qt).I) 
        #print(K)

        dState = np.matmul(K,  change)
        self.state = np.add(self.state, dState)
        
        """ Update the Covariance """
        self.proc_cov = np.subtract(np.identity(self.proc_cov.shape[0]), K) @ self.proc_cov

    def update_tdoa(self, tdoa):
        """ Function to update state with TDoA measurements.
        tdoa should be a tuple passing in the tdoa measurement:
            Two tuples of the anchor positions
            Distance difference

        NOTE: 
            We only need the error between the two, so if we're
            creative with our H matrix, we can utilize the error
            between the measured distance difference and predicted
            distance difference rather than calculating our actual
            position explicitly from the tdoa measurements.
        """
        # print("tdoa: {}".format(tdoa))
        # Anchor 1 position
        x1 = tdoa[1][0]
        y1 = tdoa[1][1]
        z1 = tdoa[1][2]

        # Anchor 0 position (not actual index, just in this measurement)
        x0 = tdoa[0][0]
        y0 = tdoa[0][1]
        z0 = tdoa[0][2]

        # differences
        dx0 = self.state[KFstate_I.X.value][0] - x0
        dy0 = self.state[KFstate_I.Y.value][0] - y0
        # dz0 = self.state[KFstate_I.Z.value][0] - z0
        dz0 = 0 - z0

        dx1 = self.state[KFstate_I.X.value][0] - x1
        dy1 = self.state[KFstate_I.Y.value][0] - y1
        # dz1 = self.state[KFstate_I.Z.value][0] - z1
        dz1 = 0 - z1

        
        # Calculate ultimate distance
        d1 = np.sqrt((dx1[0,0] ** 2) + (dy1[0,0] ** 2) + (dz1 ** 2))
        d0 = np.sqrt((dx0[0,0] ** 2) + (dy0[0,0] ** 2) + (dz0 ** 2))
        
        # Measured distance diff
        measurement = tdoa[2]

        # predicted distance diff
        predicted = d1 - d0

        self.log.debug("Predicted: {}".format(predicted))
        self.log.debug("Measurement: {}".format(measurement))
        innovation = measurement - predicted

        # Mapping state space to observation space

        dx1DivD1 = dx1[0,0] / d1
        dx0DivD0 = dx0[0,0] / d0
        dy1DivD1 = dy1[0,0] / d1
        dy0DivD0 = dy0[0,0] / d0

        # self.log.debug("{},{},{},{}".format(dx1DivD1,dx0DivD0,dy1DivD1,dy0DivD0))

        diffD1 = dx1DivD1 - dx0DivD0
        diffD0 = dy1DivD1 - dy0DivD0

        # self.log.debug("DiffD1: {}".format(diffD1))
        # self.log.debug("DiffD0: {}".format(diffD0))

        Hrow1 = [diffD1, 0, 0]
        Hrow2 = [0, diffD0, 0]
        Hrow3 = [0, 0,      0]
        # Third row is all zero to emulate that the z-axis has no effect
        # in the ground robots

        Htdoa = np.matrix([Hrow1,
                           Hrow2,
                           Hrow3])

        """ Innovation covariance Sk = Hk Pk Hk^T + Rk"""
        RkScalar = MEASUREMENT_NOISE_STD_TDOA ** 2
        Rk = np.matrix([[RkScalar, 0, 0],
                        [0, RkScalar, 0],
                        [0, 0,  RkScalar]])

        innovationCovariance = np.matmul(Htdoa, self.pred_cov)
        # self.log.debug("Innovation Covariance:\n{}".format(innovationCovariance))
        innovationCovariance = np.matmul(innovationCovariance, Htdoa.T)
        # self.log.debug("Innovation Covariance:\n{}".format(innovationCovariance))
        innovationCovariance = np.add(innovationCovariance, Rk)

        #self.log.debug("Innovation Covariance:\n{}".format(innovationCovariance))
        """ Kalman gain = Hk Pk Sk^-1 """
        kalmanGain = np.matmul(Htdoa, self.pred_cov)

     
        kalmanGain = np.matmul(kalmanGain, innovationCovariance.I)

        innovationMat = [[innovation],
                        [innovation],
                        [innovation]]
        self.log.warn("Innovation Matrix:\n{}".format(innovationMat))
        """ Update the state """
        # self.log.debug("Kalman Gain:\n{}".format(kalmanGain))
        self.log.debug("Update state with {}".format(np.matmul(kalmanGain,innovationMat)))

        self.state = np.add(self.state, np.matmul(kalmanGain, innovationMat))
        
        """ Update the process covariance """
        # self.log.debug("{}, {}".format(kalmanGain.shape, Htdoa.shape))
        kH = np.matmul(kalmanGain, Htdoa)  
        self.log.warn("KH: {}".format(kH))
        
        self.pred_cov = np.matmul(np.subtract(np.matrix([[1,0,0],[0,1,0],[0,0,1]]), kH), self.pred_cov)
        # self.pred_cov = np.matmul(np.subtract(np.identity(3), kH), self.pred_cov)
    
    
    def differential_model(self, U, dt):
        """Simple kinematic motion model for a differential steering robot.
        Returns the change in state given the control inputs. The equations 
        of motion follow:
        x = x_i + [-V/OMEGA * sin(theta)] + V/OMEGA * sin(theta + OMEGA dt) 
        y = y_i + [V/OMEGA * cos(theta)] - V/OMEGA * cos(theta + OMEGA dt)
        theta = theta_i + OMEGA * dt 
        From Probabilistic Robotics

        """
        zPrime = 0
        if U[steering.OMEGA.value, 0] != 0:
            VDivOmega = (U[steering.V.value, 0] / U[steering.OMEGA.value, 0]) 
            sumThetaOmega = self.state[KFstate_I.THETA.value, 0] + U[steering.OMEGA.value, 0] * dt

            xPrime = -VDivOmega * np.sin(self.state[KFstate_I.THETA.value, 0]) + VDivOmega * np.sin(sumThetaOmega)
            yPrime = VDivOmega * np.cos(self.state[KFstate_I.THETA.value, 0]) - VDivOmega * np.cos(sumThetaOmega)
            thetaPrime = U[steering.OMEGA.value, 0] * dt
        else:
            xPrime = U[steering.V.value, 0] * np.cos(self.state[KFstate_I.THETA.value, 0]) * dt
            yPrime = U[steering.V.value, 0] * np.sin(self.state[KFstate_I.THETA.value, 0]) * dt
            thetaPrime = 0

        return np.matrix([[xPrime],
                        [yPrime],
                        [thetaPrime],
                        [0],
                        [0]])

    # diff model but only for input w
    def differential_model_w(self, U, dt):
        if U[0, 0] != 0:
            thetaPrime = U[0, 0] * dt
        else:
            thetaPrime = 0

        return np.matrix([[0],
                        [0],
                        [thetaPrime],
                        [0],
                        [0]])

    # # diff model but only for theta and input w
    def differential_model_v(self, U, dt):
        if self.state[KFstate_I.W.value,0] != 0:
            VDivOmega = (U[0, 0] / self.state[KFstate_I.W.value, 0])
            sumThetaOmega = self.state[KFstate_I.THETA.value, 0] + self.state[KFstate_I.W.value, 0] * dt

            xPrime = -VDivOmega * np.sin(self.state[KFstate_I.THETA.value, 0]) + VDivOmega * np.sin(sumThetaOmega)
            yPrime = VDivOmega * np.cos(self.state[KFstate_I.THETA.value, 0]) - VDivOmega * np.cos(sumThetaOmega)
            #thetaPrime = U[0, 0] * dt
        else:
            xPrime = U[0, 0] * np.cos(self.state[KFstate_I.THETA.value, 0]) * dt
            yPrime = U[0, 0] * np.sin(self.state[KFstate_I.THETA.value, 0]) * dt
            #thetaPrime = 0

        return np.matrix([[xPrime],
                        [yPrime],
                        [0],
                        [0],
                        [0]])


    def differential_jacobian(self, U, dt):
        """ Returns the Jacobian of the differential model for updating 
        the process covariance estimation.
        Motion Model that is used:
        x = x_i + [-V/OMEGA * sin(theta)] + V/OMEGA * sin(theta + OMEGA dt) 
        y = y_i + [V/OMEGA * cos(theta)] - V/OMEGA * cos(theta + OMEGA dt)
        theta = theta_i + OMEGA * dt 

        From Probabilistic Robotics
        Jacobian:
        dxdth = -v/w * cos(th) + v/w cos(th + w*dt)
        dydth = -v/w * sin(th) + v/w sin(th + w*dt)
        dxdv = -1/w * sin(th) + 1/w * sin(th + w*dt)
        dxdw = (v/w^2) * sin(th) - (v/w^2) * sin(th + w*dt) + (v/w)cos(th+w*dt)*dt
        dydv = 1/w * cos(th) - 1/w*cos(th+w*dt)
        dydw = -v/(w^2) *cos(th) + (v/w^2)cos(th + w*dt) + (v/w)sin(th + w * dt) * dt
        dthdw = dt
         __                       __ 
        |   1   0   0   dxdv   dxdw |
        |   0   1   0   dydv   dydw |
        |   0   0   1   0      dt   |
        |   0   0   0   1      0    |
        |__ 0   0   0   0      1  __|
        """
        th = self.state[KFstate_I.THETA.value, 0]
        v = U[steering.V.value, 0]
        w = U[steering.OMEGA.value, 0]
        if w != 0:
            VDivOmega = (v / w) 
            OneDivOmega = 1 / w
            VDivOmegaSq = v / (w**2)
            sumThetaOmega = th + w * dt
            dxdth = -VDivOmega * np.cos(th) + VDivOmega * np.cos(sumThetaOmega)
            dydth = -VDivOmega * np.sin(th) + VDivOmega * np.sin(sumThetaOmega)
            dxdv = -OneDivOmega * np.sin(th) + OneDivOmega * np.sin(th + w * dt)
            dxdw = VDivOmegaSq * np.sin(th) \
                 - VDivOmegaSq * np.sin(th + w * dt)  \
                   + VDivOmega * np.cos(th + w * dt) * dt

            dydv = OneDivOmega * np.cos(th) - OneDivOmega * np.cos(th + w * dt)
            dydw = -VDivOmegaSq * np.cos(th) + VDivOmegaSq * np.cos(th + w * dt) + VDivOmega * np.sin(th + w * dt) * dt
            dthdw = dt 
        else:
            dxdth = -v * np.sin(th) * dt
            dydth = v * np.cos(th) * dt
            dxdv = np.cos(th) * dt
            dxdw = 0
            dydv = np.sin(th) * dt
            dydw = 0
            dthdw = 0

        return np.matrix([[1, 0, dxdth, dxdv, dxdw],
                          [0, 1, dydth, dydv, dydw],
                          [0, 0, 1, 0, dthdw],
                          [0, 0, 0, 1, 0],
                          [0, 0, 0, 0, 1]]) 

        
    def get_state(self):
        return (self.state[KFstate_I.X.value,0], self.state[KFstate_I.Y.value, 0], self.state[KFstate_I.THETA.value, 0])
        # return (self.state[KFstate_I.X.value,0], self.state[KFstate_I.Y.value, 0],self.state[KFstate_I.Z.value, 0], self.state[KFstate_I.THETA.value, 0])
