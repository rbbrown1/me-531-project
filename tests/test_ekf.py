from EKF import EKF
import pandas as pd
import numpy as np
import yaml
import matplotlib as mpt
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random
import logging

# For sampling the tdoa data
from random import choice

class DataParserTDoA():
    def __init__(self, tdoa1file, tdoa2file, anchorPosFile, stateFile, cut=None):
        tdoa1 = pd.read_csv(tdoa1file)
        tdoa2 = pd.read_csv(tdoa2file)
        self.stateEstimate = pd.read_csv(stateFile)

        # Account for differences in logging times
        # self.stateEstimate['Timestamp'] = self.stateEstimate['Timestamp'].add(18)
        # tdoa2['Timestamp'] = tdoa2['Timestamp'].add(2)

        """
            The following aligns the first timestamp for the data
        """
        # Find where state estimate first timestamp == tdoaData timestamp
        self.tdoaData = pd.merge(tdoa1, tdoa2, on='Timestamp')
        stateEstTime = self.stateEstimate.Timestamp[0]
        tdoaTime = self.tdoaData.Timestamp[0]

        if tdoaTime < stateEstTime:
            index = 0
            print("Truncating tdoa data to fit state estimate")
            for i in range(self.tdoaData.shape[0]):
                # Iterate through tdoaData until timestamp == stateEstimate first timestamp
                if stateEstTime == self.tdoaData.Timestamp[i]:
                    print(i)
                    self.tdoaData = self.tdoaData.truncate(before=i)
                    self.tdoaData = self.tdoaData.reset_index(drop=True)
                    break
        elif tdoaTime > stateEstTime:
            index = 0
            print("Aligning state estimate with tdoa data")
            for i in range(self.stateEstimate.shape[0]):
                # Iterate through stateEstimate until timestamp == tdoaData first timestamp
                if tdoaTime == self.stateEstimate.Timestamp[i]:
                    self.stateEstimate = self.stateEstimate.truncate(before=i)
                    self.stateEstimate = self.stateEstimate.reset_index(drop=True)
                    break

        print("Done...")
   
        """
            Alignment finished
        """
        """
            The following ensures that data is the same length
        """
        
        self.tdoaDataLength = self.tdoaData.shape[0]
        self.stateEstDataLength = self.stateEstimate.shape[0]

        print("Tdoa Data Length: {}".format(self.tdoaDataLength))
        print("State estimate length: {}".format(self.stateEstDataLength))
        # truncate the longer dataset to match the shorter dataset
        if (self.tdoaDataLength > self.stateEstDataLength):
            print("Truncating Tdoa data to match state estimate data length...")
            self.tdoaData = self.tdoaData.truncate(after=self.stateEstDataLength-1) 
            self.tdoaData = self.tdoaData.reset_index(drop=True)
        else: 
            print("Truncating state estimate data to match tdoa data length...")
            self.stateEstimate = self.stateEstimate.truncate(after=self.tdoaDataLength-1)
            self.stateEstimate = self.stateEstimate.reset_index(drop=True)
        self.tdoaDataLength = self.tdoaData.shape[0] # finalize the length of the data
        """
            Data cropping finished
        """
        print(self.tdoaData)
        print(self.stateEstimate)
        # Manual data cleaning 
        if cut is not None:
            self.tdoaData = self.tdoaData.truncate(before=cut).reset_index(drop=True)
            self.stateEstimate = self.stateEstimate.truncate(before=cut).reset_index(drop=True)
        
        self.tdoaDataLength = self.tdoaData.shape[0] # finalize the length of the data


        print(self.tdoaData)
        print(self.stateEstimate)
        anchor_list = []
        with open(anchorPosFile) as file:
            anchor_list = yaml.load(file, Loader=yaml.Loader)
        self.anchors = pd.DataFrame(anchor_list)
        print(self.anchors)
        # exit()


    def fetch_init(self):
        """ The kalman filter has to be held still for
        a time to allow it to converge. This initialization
        step will sample from the first data point of the
        data set only. It samples randomly between the
        dist datas (0-1, 1-2, etc.) so that it is simulated
        with localization at multiple anchors.
        """
        # Randomly sample the tdoa data
        seq = [i for i in range(0,8)]
        index = choice(seq)
        anchor0 = None
        anchor1 = None
        measurement = None
        
        # If data seems off, maybe flip anchor0 and 1?
        if index == 0:
            anchor0 = 0
            anchor1 = 1

        elif index == 1:
            anchor0 = 1
            anchor1 = 2

        elif index == 2:
            anchor0 = 2
            anchor1 = 3

        elif index == 3:
            anchor0 = 3
            anchor1 = 4

        elif index == 4:
            anchor0 = 4
            anchor1 = 5

        elif index == 5:
            anchor0 = 5
            anchor1 = 6

        elif index == 6:
            anchor0 = 6
            anchor1 = 7

        elif index == 7:
            anchor0 = 7
            anchor1 = 0
        
        # print(str(anchor0))
        # Make the position of the anchor a tuple
        pos0 = tuple(list(self.anchors.loc[:, anchor0]))
        pos1 = tuple(list(self.anchors.loc[:, anchor1]))

        measurement = self.tdoaData.iloc[0, 1 + index]
        # print("{}, {}, {}".format(pos0, pos1, measurement))
        return (pos0, pos1, measurement)


    def sample(self, sample_num, logfile):
        """ A function to sample TDoA data after the Kalman filter
        has converged.
        """
        seq = [i for i in range(0,8)] # Get a range to choose from
        index = choice(seq) # Choose a random value from the range
        anchor0 = None
        anchor1 = None
        measurement = None

        if index == 0:
            anchor0 = 0
            anchor1 = 1

        elif index == 1:
            anchor0 = 1
            anchor1 = 2

        elif index == 2:
            anchor0 = 2
            anchor1 = 3
            
        elif index == 3:
            anchor0 = 3
            anchor1 = 4

        elif index == 4:
            anchor0 = 4
            anchor1 = 5

        elif index == 5:
            anchor0 = 5
            anchor1 = 6

        elif index == 6:
            anchor0 = 6
            anchor1 = 7

        elif index == 7:
            anchor0 = 7
            anchor1 = 0

        # Make the position of the anchor a tuple
        pos0 = tuple(list(self.anchors.iloc[:, anchor0]))
        pos1 = tuple(list(self.anchors.iloc[:, anchor1]))

        # Fetch the correct measurement, Timestamp is first column
        measurement = self.tdoaData.iloc[sample_num, index + 1]
        print("Accessing ({},{}) out of {} samples".format(sample_num, index + 1, self.tdoaDataLength))

        # print("Sampling index {} at column {}".format(sample_num, index+1))
        # print("Anchor 0: {}".format(anchor0))
        # print("Anchor 1: {}".format(anchor1))
        # print("Measurement: {}".format(measurement))
        # print("Row: {}".format(self.tdoaData.iloc[sample_num]))
        logfile.write("Sampling index {} at column {}\n".format(sample_num, index+1))
        logfile.write("Anchor 0: {}\n".format(anchor0))
        logfile.write("Anchor 1: {}\n".format(anchor1))
        logfile.write("Measurement: {}\n".format(measurement))
        logfile.write("Row: {}\n".format(self.tdoaData.iloc[sample_num]))
        return (pos0, pos1, measurement)

    def expected_position(self, index):
        """ Return the crazyflie predicted value for the expected index
        """
        ## Ensure the we do not exceed the bounds of our state estimate
        if index > self.stateEstimate.shape[0] - 1:
            index = self.stateEstimate.shape[0] - 1

        x = self.stateEstimate.iloc[index, 1]
        y = self.stateEstimate.iloc[index, 2]
        return (x, y)

fig, ax = plt.subplots()
ln, = plt.plot([],[],'-ro')


def animate(num, xlist, ylist, xExpect, yExpect):
    ax.clear()
    ax.set_ylim([-1, 6])
    ax.set_xlim([-1, 6])
    plt.plot(ax = ax)
    plt.text(xlist[num], ylist[num], 'Filter')
    plt.text(xExpect[num], yExpect[num], 'Expected')
    ln.set_data([xlist[num], xExpect[num]], [ylist[num],yExpect[num]])
    return ln


if __name__ == "__main__":
    logging.basicConfig(level=logging.CRITICAL)
    f = EKF()

    # parser = DataParserTDoA('./sample_data/Set1/TdoaD1-20200324T15-34-06.csv','./sample_data/Set1/TdoaD2-20200324T15-34-06.csv', './sample_data/Set1/anchor_positions_homelab.yaml', './sample_data/Set1/stateEstimate-20200324T15-34-08.csv')

    parser = DataParserTDoA('./sample_data/Set0/TdoaD1-20200131T17-04-42.csv','./sample_data/Set0/TdoaD2-20200131T17-04-42.csv', './sample_data/Set0/anchor_positions.yaml', './sample_data/Set0/stateEstimate-20200131T17-04-43.csv', cut=8000)
    
    log = open("PositionLog.log", "w+")
    xlist = []
    xExpect = []
    ylist = []
    yExpect = []
    time = []
    ax.autoscale(enable=True, axis='both')
    initSamples = 1000
    numSamples = parser.tdoaDataLength 
    for i in range(initSamples):
        time.append(i)
        print("Init: ")
        f.print_state()
        xlist.append(f.get_state()[0])
        ylist.append(f.get_state()[1])
        steering = np.matrix([[0],[0]])
        f.predict(steering)
        print("State: {}".format(f.get_state()))
        f.print_covariance()

        log.write("State: ")
        log.write("{}\n".format(f.get_state()))

        yExpect.append(parser.expected_position(0)[1])
        xExpect.append(parser.expected_position(0)[0])
        # f.print_state()
        tdoa = parser.sample(0, log)
        log.write("{}\n".format(tdoa))
        f.update_tdoa_scalar(tdoa)

    prev_speed = 0
    prev_dir = 0
    for i in range(numSamples):
        time.append(i + initSamples) # adding initSamples for data continuity
        xlist.append(f.get_state()[0])
        ylist.append(f.get_state()[1])


        # Calculate the velocity and position
        expected_position = parser.expected_position(i);
        next_expected_position = parser.expected_position(i+1);
        x_vel = next_expected_position[0] - expected_position[0]
        y_vel = next_expected_position[1] - expected_position[1]

        forward_vel = np.sqrt(x_vel ** 2 + y_vel ** 2)

        bearing = np.arctan2(y_vel, x_vel)
        rot_vel = bearing - prev_dir

        steering = np.matrix([[forward_vel], [rot_vel]])
        f.predict(steering)
        tdoa = parser.sample(i, log)
        expected_position = parser.expected_position(i)

        yExpect.append(parser.expected_position(i)[1])
        xExpect.append(parser.expected_position(i)[0])
 
        f.update_tdoa_scalar(tdoa)
        # f.print_covariance()
        if np.isnan(f.get_state()[0]):
            break
        log.write("{}\n".format(tdoa))
        log.write("State: ")
        log.write("{}".format(f.get_state()))
        f.print_state()
        log.write(" Expected: X: {}, Y: {}\n".format(expected_position[0], expected_position[1]))
        # print("State: {}".format(f.get_state()))
        print("Expected: X: {}, Y: {}\n".format(expected_position[0], expected_position[1]))
        prev_speed = forward_vel
        prev_dir = bearing 

    log.close()
    fig, (ax1, ax2) = plt.subplots(2)
    
    ax1.plot(time,ylist, time,yExpect)
    ax1.set_title("Expected and Filtered Y Position Over Time")
    ax1.set(xlabel = "Time (s)", ylabel= "Y position(m)")
    ax1.legend(["Filtered", "Expected"])
    
    ax2.plot(time,xlist, time, xExpect)
    ax2.set_title("Expected and Filtered X Position Over Time")
    ax2.set(ylabel ="X position (m)", xlabel = "Time (s)")
    ax2.legend(["Filtered", "Expected"])
    
    positionDF = pd.DataFrame(
        {'xFilter': xlist,
         'yFilter': ylist,
         'xExpect': xExpect,
         'yExpect': yExpect}
    )
    
    positionCovariance = positionDF.cov()
    logging.info(positionCovariance)

    print(positionCovariance)
    # anim = animation.FuncAnimation(fig, animate, numSamples + initSamples, fargs=(xlist, ylist, xExpect, yExpect), interval=0.6, blit = False)

    # ax.set_ylim([-10, 10])
    # ax.set_xlim([-10, 10])
    plt.show()
    